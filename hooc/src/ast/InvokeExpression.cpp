/*
 * Copyright 2019 Benoy Bose
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <utility>

#include "InvokeExpression.hh"

namespace hooc {
    namespace ast {
        InvokeExpression::InvokeExpression(Expression *receiver, std::list<Expression *> arguments,
                                           ParserRuleContext *context,
                                           const std::string &file_name) :
                Expression(EXPRESSION_INVOKE, context, file_name),
                _receiver(receiver),
                _arguments(std::move(arguments)) {
        }

        InvokeExpression::~InvokeExpression() {
            delete _receiver;
            while (_arguments.begin() != _arguments.end()) {
                auto arg = *(_arguments.begin());
                _arguments.remove(arg);
                delete arg;
            }
        }

        const Expression *InvokeExpression::GetReceiver() const {
            return this->_receiver;
        }

        const std::list<Expression *> InvokeExpression::GetArguments() const {
            return this->_arguments;
        }
    }
}
